import java.util.Scanner;

import static java.lang.Math.*;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj tresc wiadomosci do skrocenia:");

        String wiadomosc = scanner.nextLine();
        String nowa = przeksztalcanieWiadomosci(wiadomosc);

        System.out.println();
        System.out.println("Twoj tekst zostanie podzielony na: " + podzielNaIloscWiadomosci(nowa.length()) + " wiadomosci.");
    }


    private static String przeksztalcanieWiadomosci(String wiadomosc){
        wiadomosc = wiadomosc.replaceAll("\\s+", " ");
        String[] slowa = wiadomosc.split(" ");
        int sumaZnakow = 0;
        String nowa = "";

        for (String slowo : slowa) {
            nowa = slowo.substring(0, 1).toUpperCase() + slowo.substring(1);
            sumaZnakow += nowa.length();
            System.out.print(nowa);
        }
        return nowa;
    }


    private static int podzielNaIloscWiadomosci(int sumaZnakow) {
        int iloscWiadomosci = 1;

        if (sumaZnakow <= 160) {
            return iloscWiadomosci;
        } else {
            iloscWiadomosci = (int) ceil((double) sumaZnakow / 153);
        }
        return iloscWiadomosci;
    }
}
